<?php
	$longUrl=trim($_POST['url']);
	$type=trim($_POST['type']);
	//如果没有网址自动加上 http://
	if(stripos($longUrl, '://')===false){
		$longUrl='http://'.$longUrl;
	}
	
	include "TinyUrl.class.php";
	$tu=new TinyUrl();
	$tu->url=$longUrl;
	
	switch ($type){
		case 'd5.nz':
			$tinyurl=$tu->created5nz();
			break;
		case 'dd5.org':
			$tinyurl=$tu->createdd5org();
			break;
		case 'duan.es':
			$tinyurl=$tu->createduanes();
			break;
		case 'durl.xyz':
			$tinyurl=$tu->createdurlxyz();
			break;
		default:
			$tinyurl=$tu->created5nz();
			break;
	}	
	
	
	
	if(!$tinyurl){
		responseMesg(0,$tu->error);
	}else{
		responseMesg(1,'成功',$tinyurl);
	}
	
	//返回json信息
	function responseMesg($code=0,$message,$data=null){
		$arr=array();
		$arr['code']=$code;
		$arr['message']=$message;
		$arr['data']=$data;
		echo json_encode($arr);
		//结束
		exit();
	}
