<?php
/**
 * 生成短网址
 * 
 * @author silenceper
 *
 */
class TinyUrl{
	public $url=null;
	//d5.nz key 申请地址：https://d5.nz/
	public $d5key='';

	public $error;
	
	
	/**
	 * 生成d5.nz短网址
	 */
	public function created5nz(){
		$source=$this->d5key;
		$url_long=$this->url;
		$url="https://d5.nz/api/?key=$source&url=$url_long";
		$res=$this->_curl($url);
		if(!$res){
			return false;
		}
		$json=json_decode($res);
		if($json->error=='1'){
			$this->error=$json->msg;
			return false;
		}else{
			return $json->short;
		}
	}
	
	/**
	 * 生成dd5.org 网址
	 */
	public function createdd5org(){
		$source=$this->d5key;
		$url_long=$this->url;
		$url="https://d5.nz/api/?key=$source&url=$url_long&domain=https://dd5.org";
		$res=$this->_curl($url);
		if(!$res){
			return false;
		}
		$json=json_decode($res);
		if($json->error=='1'){
			$this->error=$json->msg;
			return false;
		}else{
			return $json->short;
		}
	}
	
	/**
	 * 生成duan.es网址
	 */
	public function createduanes(){
		$source=$this->d5key;
		$url_long=$this->url;
		$url="https://d5.nz/api/?key=$source&url=$url_long&domain=https://duan.es";
		$res=$this->_curl($url);
		if(!$res){
			return false;
		}
		$json=json_decode($res);
		if($json->error=='1'){
			$this->error=$json->msg;
			return false;
		}else{
			return $json->short;
		}
	}
	
	
	
	/**
	 * 生成durl.xyz网址
	 */
	public function createdurlxyz(){
		$source=$this->d5key;
		$url_long=$this->url;
		$url="https://d5.nz/api/?key=$source&url=$url_long&domain=https://durl.xyz";
		$res=$this->_curl($url);
		if(!$res){
			return false;
		}
		$json=json_decode($res);
		if($json->error=='1'){
			$this->error=$json->msg;
			return false;
		}else{
			return $json->short;
		}
	}
	


	
	
	/**
	 * 发送http请求
	 */
	public function _curl($url,$method='get',$data=null){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10000);
		curl_setopt($ch, CURLOPT_TIMEOUT, 500);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if($method=='post'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		}
		$result = curl_exec($ch);
		curl_close($ch);
		if(!$result){
			//curl 出现错误
			return false;
		}
		return $result;
	}
	
	
}

?>